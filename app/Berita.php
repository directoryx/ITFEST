<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Model;

class Berita extends Model
{
    use Notifiable;

    protected $table = "berita";
    protected $fillable = [
        'namaberita','isi',
    ];
}
