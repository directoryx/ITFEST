<?php


namespace App\Http\Middleware;


use Closure;
use Auth;
use Session;
use DB;

class CheckUpdateTim

{

    /**

     * Handle an incoming request.

     *

     * @param  \Illuminate\Http\Request  $request

     * @param  \Closure  $next

     * @return mixed

     */

    public function handle($request, Closure $next)

    {        
        if (Auth::user() != null){
            $users = DB::table('dataanggota')            
                ->where("kelompokid", Auth::user()->id)
                ->count();
            if (Auth::user()->status < 2 || $users < 2) {
                Session::flash('message', "Mohon Untuk Diisi terlebih dahulu data berikut");
                return redirect(url("updatedata"));
            }
        } else {
            return redirect(url("login"));
        }

        
        
        return $next($request);

    }

}