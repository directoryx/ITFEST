<?php


namespace App\Http\Middleware;


use Closure;
use Auth;
use DB;
use Session;

class CheckProposal

{

    /**

     * Handle an incoming request.

     *

     * @param  \Illuminate\Http\Request  $request

     * @param  \Closure  $next

     * @return mixed

     */

    public function handle($request, Closure $next)

    {
        if (Auth::user() != null){
            $users = DB::table('dataanggota')            
                ->where("kelompokid", Auth::user()->id)
                ->count();
            $userscount = DB::table('dataupload')            
                ->where("kelompokid", Auth::user()->id)
                ->count();
            $usersnull = DB::table('dataupload')            
                ->where("kelompokid", Auth::user()->id)            
                ->first();
            
            if (Auth::user()->status < 2 || $users < 2 || $usersnull == NULL || $userscount == 0) {
                Session::flash('message', "Link proposal harus diisi terlebih dahulu");
                return redirect(url("uploadproposal"));
            }
        } else {
            return redirect(url("login"));
        }
        
        
        return $next($request);

    }

}