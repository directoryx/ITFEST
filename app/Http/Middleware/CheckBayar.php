<?php


namespace App\Http\Middleware;


use Closure;
use Auth;
use Session;

class CheckBayar

{

    /**

     * Handle an incoming request.

     *

     * @param  \Illuminate\Http\Request  $request

     * @param  \Closure  $next

     * @return mixed

     */

    public function handle($request, Closure $next)

    {
        if (Auth::user() != null){
            if (Auth::user()->status < 2) {
                Session::flash('message', "Mohon Lakukan Pembayaran terlebih dahulu dan tunggu verifikasi dari panitia");
                return redirect(url("uploadkwin"));
            }
        } else {
            return redirect(url("login"));
        }
        
        
        return $next($request);

    }

}