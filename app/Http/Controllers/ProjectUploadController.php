<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Auth;
use App\User;

class ProjectUploadController extends Controller
{
    public function __construct()
    {
        $this->middleware('cekproposal');
    }

    public function index(){
        $user = new User();
        $stat = $user->cekproject();
        $linkproposal = $user->getLinkDriveProject();
        
        return view("uploadproject",compact("stat"),compact("linkproposal"));
    }

    public function uploadproject(Request $request){        
        DB::table('dataupload')
            ->where('kelompokid', Auth::user()->id)
            ->update(['linkproject' => $request->linkproject]);
        return redirect()->back();
    }
}
