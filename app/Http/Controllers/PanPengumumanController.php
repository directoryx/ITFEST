<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Berita;
use Validator;
use Illuminate\Support\Facades\Input;
use Session;

class PanPengumumanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('pan');
    }

    public function index()
    {
        $berita = Berita::all();
        return view("pan.pengumuman")->with("berita",$berita);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view("pan.createpengumuman");
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = array(
            'nama'       => 'required',
            'editor1'      => 'required',
        );
        $validator = Validator::make(Input::all(), $rules);

        // process the login
        if ($validator->fails()) {
            return Redirect::to('pengumuman/create')
                ->withErrors($validator);
        } else {
            // store
            $nerd = new Berita;
            $nerd->namaberita       = $request->nama;
            $nerd->isi      = $request->editor1;            
            $nerd->save();

            // redirect
            Session::flash('message', 'Successfully created nerd!');
            return Redirect()->back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $nerd = Berita::find($id);
        $nerd->delete();

        // redirect
        Session::flash('message', 'Berhasil Menghapus Pengumuman!');
        return Redirect()->back();
    }
}
