<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Auth;
use Session;

class DataTimController extends Controller
{
    public function __construct()
    {
        $this->middleware('cekbayar');
    }

    public function index(){
        $users = DB::table('users')->select('namaketua', 'namatim','universitas')
            ->where("id",Auth::user()->id)
            ->first();
        $users1 = DB::table('dataanggota')
            ->where("kelompokid",Auth::user()->id)
            ->count();
        //print_r($users);
        return view("datatim",compact("users"),compact("users1"));
    }


    public function update(Request $request){
        //print_r($request->emailanggota);
        $keys = array_keys($request->emailanggota);
        $last = end($keys);

        $keys1 = array_keys($request->anggota);
        $last1 = end($keys);

        $keys2 = array_keys($request->notelpanggota);
        $last2 = end($keys);
        $stat = 0;
        $stat1 = 0;
        $stat2 = 0;
        for ($i=0;$i<=$last;$i++) {
            if (empty($request->emailanggota[$i])){
                $stat = $stat + 1;
            }
        }
        for ($i=0;$i<=$last1;$i++) {
            if (empty($request->anggota[$i])){
                $stat1 = $stat1 + 1;
            }
        }

        for ($i=0;$i<=$last2;$i++) {
            if (empty($request->notelpanggota[$i])){
                $stat2 = $stat2 + 1;
            }
        }
        $count = DB::table('dataanggota')
            ->where('kelompokid', Auth::user()->id)
            ->count();

        //echo $stat;
        //echo $stat1;
        //echo $stat2;
        if ($count == 0 ){     
            if ($stat == 0 && $stat1 == 0  && $stat2 == 0){        
                for ($i=0;$i<=$last;$i++) {

                    DB::table('dataanggota')->insert(
                        ['kelompokid' => Auth::user()->id ,'namaanggota' => $request->anggota[$i], 'emailanggota' => $request->emailanggota[$i], 'notelpanggota' => $request->notelpanggota[$i]]
                    );
                }
                return redirect()->back();
            } else {
                Session::flash('message', "Semua Input Harus Diisi");
                return redirect()->back();
            }
        } else {
            Session::flash('message', "Data Telah Ada");
            return redirect()->back();
        }
        
        
    }
}
