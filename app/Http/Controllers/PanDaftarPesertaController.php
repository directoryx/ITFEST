<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use DB;
use Illuminate\Support\Facades\Crypt;
use Session;

class PanDaftarPesertaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $this->middleware('pan');
    }

    public function index()
    {
        $user = User::all();
        return view('pan.peserta', compact('user'));
        //return view("pan.peserta");
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        if(isset($request->kelompokid) && isset($request->act)){
            $kelompokid_dec = Crypt::decryptString($request->kelompokid);
            $act_dec = Crypt::decryptString($request->act);
            if($act_dec == "approve"){
                DB::table('users')
                    ->where('id', $kelompokid_dec)
                    ->update(['status' => 2]);
                Session::flash('message', "Berhasil mensetujui");
                return redirect()->back();
            } elseif($act_dec == "delete"){
                DB::table('bayar')
                    ->where('kelompokid', '=', $kelompokid_dec)
                    ->delete();
                $users = DB::table('users')
                    ->where("id",$kelompokid_dec)
                    ->first();
                if ($users->status == 2){
                    DB::table('users')
                    ->where('id', $kelompokid_dec)
                    ->update(['status' => 1]);
                }
                
                Session::flash('message', "Berhasil menghapus file");
                return redirect()->back();
            }

        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = DB::table('users')           
            ->where("users.id",$id)
            ->first();

        $dataanggota = DB::table('dataanggota')           
            ->where("dataanggota.kelompokid",$id)
            ->get();
        $databayar = DB::table('bayar')           
            ->where("kelompokid",$id)
            ->first();
        

        $dataupload = DB::table('dataupload')           
            ->where("kelompokid",$id)
            ->first();
        //print_r($user->namatim);
        //print_r($dataanggota);
        //print_r($databayar);
        //print_r($dataupload);
        $kelompokid_enc = Crypt::encryptString($id);
        $approve_enc = Crypt::encryptString("approve");
        $delete_enc = Crypt::encryptString("delete");
        return view("pan.detailtim",compact("user"),compact("dataanggota"))->with("dataupload",$dataupload)->with("databayar",$databayar)->with("kelompokid_enc",$kelompokid_enc)->with("approve_enc",$approve_enc)->with("delete_enc",$delete_enc);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
