<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use DB;
use Auth;
use Session;
use App\User;

class UploadBerkasController extends Controller
{
    public function __construct()
    {
        $this->middleware('cekupdatetim');
    }

    public function index(){
        $user = new User();
        $stat = $user->cekproposal();
        $linkproposal = $user->getLinkDrive();
        $usersnull = DB::table('dataupload')            
                ->where("kelompokid", Auth::user()->id)            
                ->get();
        //print_r($usersnull);
        return view("uploadproposal",compact("stat"),compact("linkproposal"));
    }


    public function uploadproposal(Request $request){
        $count = DB::table('dataupload')
            ->where('kelompokid', Auth::user()->id)
            ->count();
        if ($count == 0){
            DB::table('dataupload')->insert(
                ['kelompokid' => Auth::user()->id ,'linkproposal' => $request->linkproposal]
            );
            return redirect()->back();
        } else {
            Session::flash('message', "Data Telah Ada");
            return redirect()->back();
        }        
        
    }

    
}
