<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Session;
use Auth;
use Validator;
use Illuminate\Support\Facades\Input;
use App\User;

class UploadController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(){
        $user = new User();
        $stat = $user->cekupload();
        return view("uploadbayar",compact("stat"));
    }

    public function upload(Request $request){
        $count = DB::table('bayar')
            ->where('kelompokid', Auth::user()->id)
            ->count();
        $rules = array(
            'image'       => 'required|mimes:jpeg,jpg,png',            
        );
        $validator = Validator::make(Input::all(), $rules);
    
            // process the login
        if ($validator->fails()) {
            Session::flash('message', "Pastikan Format yang anda upload");
            return redirect()->back();
        } else {
            if ($count == 0) {
                $file = $request->file('image');
                $destinationPath = 'uploads';
                $random = rand(10000,99999);
                $url_en = "uploads/".$random.".".$file->getClientOriginalExtension();
                DB::table('bayar')->insert(
                    ['kelompokid' => Auth::user()->id, 'url' => $url_en]
                );
                $file->move($destinationPath,$random.".".$file->getClientOriginalExtension());
                return redirect()->back();
            } else {
                Session::flash('message', "Data Telah Ada");
                return redirect()->back();
            }
        }
        
    }
}
