<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use DB;
use Auth;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'namaketua','namatim','nohp','universitas', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function cekupload(){
        $userid = Auth::user()->id;
        $userstatus = Auth::user()->status;
        $users = DB::table('bayar')            
            ->where("kelompokid", Auth::user()->id)
            ->count()
        ;
        if($userstatus == 1 && $users == 1){
            return 1;
        } else if($userstatus == 2 && $users == 1) {
            return 2;
        } else {
            return 0;
        }
    }

    public function cekproposal(){
        $userid = Auth::user()->id;
        $userstatus = Auth::user()->status;
        $userscount = DB::table('dataupload')            
            ->where("kelompokid", Auth::user()->id)
            ->count();
        $usersnull = DB::table('dataupload')            
            ->where("kelompokid", Auth::user()->id)            
            ->first();
        if ($userscount != 0){
            if($usersnull->linkproposal != NULL && $userscount == 1){
                return 1;
            } else {
                return 0;
            }
        }
        
    }

    public function getLinkDrive(){
        $userid = Auth::user()->id;
        $userstatus = Auth::user()->status;  
        $datauploadambil = DB::table('dataupload')            
            ->where("kelompokid", Auth::user()->id)            
            ->first();
        if ($datauploadambil != null){            
            $linkproposal = $datauploadambil->linkproposal;
        }   else {
            $linkproposal = 0;
        }
        
        return $linkproposal;
    }

    public function cekproject(){
        $userid = Auth::user()->id;
        $userstatus = Auth::user()->status;
        $userscount = DB::table('dataupload')            
            ->where("kelompokid", Auth::user()->id)
            ->count();
        $usersnull = DB::table('dataupload')            
            ->where("kelompokid", Auth::user()->id)            
            ->first();
        if ($userscount != 0){
            if($usersnull->linkproject != NULL && $userscount == 1){
                return 1;
            } else {
                return 0;
            }
        }
        
    }


    public function getLinkDriveProject(){
        $userid = Auth::user()->id;
        $userstatus = Auth::user()->status;  
        $datauploadambil = DB::table('dataupload')            
            ->where("kelompokid", Auth::user()->id)            
            ->first();
        if ($datauploadambil != null){            
            $linkproposal = $datauploadambil->linkproject;
        }   else {
            $linkproposal = 0;
        }
        
        return $linkproposal;
    }
}
