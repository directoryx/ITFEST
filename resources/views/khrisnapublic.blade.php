@extends('layouts.khrisna')

@section('content')

    <!-- header

    ================================================== -->

    <header class="s-header">



            <div class="header-logo">
    
                <a class="site-logo" href="index.html">
    
                    <img style="width:200px;height:200px" src="images/integer.png" alt="Homepage">
    
                </a>
    
            </div> <!-- end header-logo -->
    
    
    
            <nav class="header-nav">
    
    
    
                <a href="#0" class="header-nav__close" title="close"><span>Close</span></a>
    
    
    
                <div class="header-nav__content">
    
                    <h3>Transcen Studio</h3>
    
    
    
                    <ul class="header-nav__list">
    
                        <li class="current"><a href="{{url("login")}}" title="Login">Login</a></li>
    
                        <li class="current"><a class="smoothscroll"  href="#home" title="home">Home</a></li>
    
                        <li><a class="smoothscroll"  href="#about" title="about">About</a></li>
    
                        <li><a class="smoothscroll"  href="#services" title="services">Alur kompetensi</a></li>
    
                        <li><a class="smoothscroll"  href="#works" title="works">Apa Kata?</a></li>
    
                        <li><a class="smoothscroll"  href="#stats" title="contact">Register</a></li>
    
                        <li><a class="smoothscroll"  href="#contact" title="contact">Contact</a></li>
    
                    </ul>
    
    
    
                    <p>Perspiciatis hic praesentium nesciunt. Et neque a dolorum <a href='#0'>voluptatem</a> porro iusto sequi veritatis libero enim. Iusto id suscipit veritatis neque reprehenderit.</p>
    
    
    
                    <ul class="header-nav__social">
    
                        <li>
    
                            <a href="#0"><i class="fab fa-facebook"></i></a>
    
                        </li>
    
                        <li>
    
                            <a href="#0"><i class="fab fa-twitter"></i></a>
    
                        </li>
    
                        <li>
    
                            <a href="#0"><i class="fab fa-instagram"></i></a>
    
                        </li>
    
                        <li>
    
                            <a href="#0"><i class="fab fa-behance"></i></a>
    
                        </li>
    
                        <li>
    
                            <a href="#0"><i class="fab fa-dribbble"></i></a>
    
                        </li>
    
                    </ul>
    
    
    
                </div> <!-- end header-nav__content -->
    
    
    
            </nav> <!-- end header-nav -->
    
    
    
            <a class="header-menu-toggle" href="#0">
    
                <span class="header-menu-icon"></span>
    
            </a>
    
    
    
        </header> <!-- end s-header -->
    
    
    
    
    
        <!-- home
    
        ================================================== -->
    
        <section id="home" class="s-home target-section" data-parallax="scroll" data-image-src="images/hero-bg.jpg" data-natural-width=3000 data-natural-height=2000 data-position-y=top>
    
    
    
            <div class="shadow-overlay"></div>
    
    
    
            <div class="home-content">
    
    
    
                <div class="row home-content__main">
    
                    <h1>
    
                    Hello ,your in <br>
    
                    INTEGER Competion.
    
                    </h1>
    
    
    
                    <p>
    
                    We create stunning digital experiences <br>
    
                    that will help your business stand out.
    
                    </p>
    
                </div> <!-- end home-content__main -->
    
    
    
            </div> <!-- end home-content -->
    
    
    
            <ul class="home-sidelinks">
    
                <li><a href="{{url("login")}}">Login<span>who we are</span></a></li>
    
                <li><a class="smoothscroll" href="#about">About<span>who we are</span></a></li>
    
                <li><a class="smoothscroll" href="#services">Alur Kompetisi<span>what we do</span></a></li>
    
                <li><a class="smoothscroll" href="#works">Apa Kata ?<span>what we do</span></a></li>
    
                <li><a  class="smoothscroll" href="#stats">Register<span>get in touch</span></a></li>
    
                <li><a  class="smoothscroll" href="#contact">Contact<span>get in touch</span></a></li>
    
            </ul> <!-- end home-sidelinks -->
    
    
    
            <ul class="home-social">
    
                <li class="home-social-title">Follow Us</li>
    
                <li><a href="#0">
    
                    <i class="fab fa-facebook"></i>
    
                    <span class="home-social-text">Facebook</span>
    
                </a></li>
    
                <li><a href="#0">
    
                    <i class="fab fa-twitter"></i>
    
                    <span class="home-social-text">Twitter</span>
    
                </a></li>
    
                <li><a href="#0">
    
                    <i class="fab fa-linkedin"></i>
    
                    <span class="home-social-text">LinkedIn</span>
    
                </a></li>
    
            </ul> <!-- end home-social -->
    
    
    
            <a href="#about" class="home-scroll smoothscroll">
    
                <span class="home-scroll__text">Scroll Down</span>
    
                <span class="home-scroll__icon"></span>
    
            </a> <!-- end home-scroll -->
    
    
    
        </section> <!-- end s-home -->
    
    
    
    
    
        <!-- about
    
        ================================================== -->
    
        <section id='about' class="s-about">
    
    
    
            <div class="row section-header" data-aos="fade-up">
    
                <div class="col-full">
    
                    <h3 class="subhead">Who We Are</h3>
    
                    <h1 class="display-1">We are a group of design driven individuals passionate about creating beautiful UI designs.</h1>
    
                </div>
    
            </div> <!-- end section-header -->
    
    
    
            <div class="row" data-aos="fade-up">
    
                <div class="col-full">
    
                    <p class="lead">
    
                    Curabitur arcu erat, accumsan id imperdiet et, porttitor at sem. Donec rutrum congue leo eget malesuada. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse.
    
                    </p>
    
                </div>
    
            </div> <!-- end about-desc -->
    
    
    
    <!-- START KOMPETISI -->
    
            <div class="row">
    
              <div class="col-six tab-full">
    
                <h3>Kompetisi 1</h3>
    
                  <p><a href="#"><img width="120" height="120" class="pull-left" alt="sample-image" src="images/sample-image.jpg"></a>
    
                  Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Donec libero. Suspendisse bibendum.Cras id urna. Morbi tincidunt, orci ac convallis aliquam, lectus turpis varius lorem, eu posuere nunc justo tempus leo. Donec mattis, purus nec placerat bibendum, dui pede condimentum odio, ac blandit ante orci ut diam. Cras fringilla magna. Phasellus suscipit, leo a pharetra condimentum, lorem tellus eleifend magna, eget fringilla velit magna id neque posuere nunc justo tempus leo. </p>
    
              </div>
    
              <div class="col-six tab-full">
    
                <h3>Kompetisi 2</h3>
    
                  <p><a href="#"><img width="120" height="120" class="pull-left" alt="sample-image" src="images/sample-image.jpg"></a>
    
                  Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Donec libero. Suspendisse bibendum.Cras id urna. Morbi tincidunt, orci ac convallis aliquam, lectus turpis varius lorem, eu posuere nunc justo tempus leo. Donec mattis, purus nec placerat bibendum, dui pede condimentum odio, ac blandit ante orci ut diam. Cras fringilla magna. Phasellus suscipit, leo a pharetra condimentum, lorem tellus eleifend magna, eget fringilla velit magna id neque posuere nunc justo tempus leo. </p>
    
              </div>
    
    
    
            </div>
    
    
    
                    <div class="row">
    
                      <div class="col-six tab-full">
    
                        <h3>Kompetisi 3</h3>
    
                          <p><a href="#"><img width="120" height="120" class="pull-left" alt="sample-image" src="images/sample-image.jpg"></a>
    
                          Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Donec libero. Suspendisse bibendum.Cras id urna. Morbi tincidunt, orci ac convallis aliquam, lectus turpis varius lorem, eu posuere nunc justo tempus leo. Donec mattis, purus nec placerat bibendum, dui pede condimentum odio, ac blandit ante orci ut diam. Cras fringilla magna. Phasellus suscipit, leo a pharetra condimentum, lorem tellus eleifend magna, eget fringilla velit magna id neque posuere nunc justo tempus leo. </p>
    
                      </div>
    
    
    
                      <div class="col-six tab-full">
    
                        <h3>Kompetisi 4</h3>
    
                          <p><a href="#"><img width="120" height="120" class="pull-left" alt="sample-image" src="images/sample-image.jpg"></a>
    
                          Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Donec libero. Suspendisse bibendum.Cras id urna. Morbi tincidunt, orci ac convallis aliquam, lectus turpis varius lorem, eu posuere nunc justo tempus leo. Donec mattis, purus nec placerat bibendum, dui pede condimentum odio, ac blandit ante orci ut diam. Cras fringilla magna. Phasellus suscipit, leo a pharetra condimentum, lorem tellus eleifend magna, eget fringilla velit magna id neque posuere nunc justo tempus leo. </p>
    
                      </div>
    
    
    
                    </div>
    
                    <!-- END KOMPETISI  -->
    
    
    
        </section> <!-- end s-about -->
    
    
    
    
    
        <!-- PROSES KOMPETISI
    
        ================================================== -->
    
        <section id='services' class="s-services light-gray">
    
    
    
            <div class="row section-header" data-aos="fade-up">
    
                <div class="col-full">
    
                    <h3 class="subhead">Alur Kompetisi</h3>
    
            </div> <!-- end section-header -->
    
            <br>
    
            <div class="about-process process block-1-2 block-tab-full">
    
    
    
                <div class="process__vline-left"></div>
    
                <div class="process__vline-right"></div>
    
    
    
                            <div class="col-block process__col" data-item="1" data-aos="fade-up">
    
                                <div class="process__text">
    
                                    <h4>Define</h4>
    
    
    
                                    <p>
    
                                    Quos dolores saepe mollitia deserunt accusamus autem reprehenderit. Voluptas facere animi explicabo non quis magni recusandae.
    
                                    Numquam debitis pariatur omnis facere unde. Laboriosam minus amet nesciunt est. Et saepe eos maxime tempore quasi deserunt ab.
    
                                    </p>
    
                                </div>
    
                            </div>
    
                            <div class="col-block process__col" data-item="2" data-aos="fade-up">
    
                                <div class="process__text">
    
                                    <h4>Design</h4>
    
    
    
                                    <p>
    
                                    Quos dolores saepe mollitia deserunt accusamus autem reprehenderit. Voluptas facere animi explicabo non quis magni recusandae.
    
                                    Numquam debitis pariatur omnis facere unde. Laboriosam minus amet nesciunt est. Et saepe eos maxime tempore quasi deserunt ab.
    
                                    </p>
    
                                </div>
    
                            </div>
    
                            <div class="col-block process__col" data-item="3" data-aos="fade-up">
    
                                <div class="process__text">
    
                                    <h4>Build</h4>
    
    
    
                                    <p>
    
                                    Quos dolores saepe mollitia deserunt accusamus autem reprehenderit. Voluptas facere animi explicabo non quis magni recusandae.
    
                                    Numquam debitis pariatur omnis facere unde. Laboriosam minus amet nesciunt est. Et saepe eos maxime tempore quasi deserunt ab.
    
                                    </p>
    
                                </div>
    
                            </div>
    
                            <div class="col-block process__col" data-item="4" data-aos="fade-up">
    
                                <div class="process__text">
    
                                    <h4>Launch</h4>
    
    
    
                                    <p>
    
                                    Quos dolores saepe mollitia deserunt accusamus autem reprehenderit. Voluptas facere animi explicabo non quis magni recusandae.
    
                                    Numquam debitis pariatur omnis facere unde. Laboriosam minus amet nesciunt est. Et saepe eos maxime tempore quasi deserunt ab.
    
                                    </p>
    
                                </div>
    
                            </div>
    
    </div>
    
            </div>
    
    
    
        </section>
    
    
    
        <!--- END ALUR KOMPETISI -->
    
    
    
        <!-- works
    
        ================================================== -->
    
        <section id='works' class="s-works">
    
    
    
            <div class="row section-header" data-aos="fade-up">
    
                <div class="col-full">
    
                    <h3 class="subhead">Apa Kata ?</h3>
    
                </div>
    
            </div> <!-- end section-header -->
    
    
    
            <div class="testimonials-wrap" data-aos="fade-up">
    
    
    
                <div class="row testimonials">
    
    
    
                    <div class="col-full testimonials__slider">
    
    
    
                        <div class="testimonials__slide">
    
                            <img src="images/avatars/user-01.jpg" alt="Author image" class="testimonials__avatar">
    
                            <p>Qui ipsam temporibus quisquam velMaiores eos cumque distinctio nam accusantium ipsum.
    
                            Laudantium quia consequatur molestias delectus culpa facere hic dolores aperiam. Accusantium quos qui praesentium corpori.</p>
    
                            <div class="testimonials__author">
    
                                Tim Cook
    
                                <span>CEO, Apple</span>
    
                            </div>
    
                        </div> <!-- end testimonials__slide -->
    
    
    
                        <div class="testimonials__slide">
    
                            <img src="images/avatars/user-05.jpg" alt="Author image" class="testimonials__avatar">
    
                            <p>Excepturi nam cupiditate culpa doloremque deleniti repellat. Veniam quos repellat voluptas animi adipisci.
    
                            Nisi eaque consequatur. Quasi voluptas eius distinctio. Atque eos maxime. Qui ipsam temporibus quisquam vel.</p>
    
                            <div class="testimonials__author">
    
                                Sundar Pichai
    
                                <span>CEO, Google</span>
    
                            </div>
    
                        </div> <!-- end testimonials__slide -->
    
    
    
                        <div class="testimonials__slide">
    
                            <img src="images/avatars/user-02.jpg" alt="Author image" class="testimonials__avatar">
    
                            <p>Repellat dignissimos libero. Qui sed at corrupti expedita voluptas odit. Nihil ea quia nesciunt. Ducimus aut sed ipsam.
    
                            Autem eaque officia cum exercitationem sunt voluptatum accusamus. Quasi voluptas eius distinctio.</p>
    
                            <div class="testimonials__author">
    
                                Satya Nadella
    
                                <span>CEO, Microsoft</span>
    
                            </div>
    
                        </div> <!-- end testimonials__slide -->
    
    
    
                    </div> <!-- end testimonials__slider -->
    
    
    
                </div> <!-- end testimonials -->
    
    
    
            </div> <!-- end testimonials-wrap -->
    
    
    
        </section> <!-- end s-works -->
    
    
    
    
    
        <!-- stats
    
        ================================================== -->
    
        <section id="stats" class="s-stats">
    
    
    
            <div class="row stats block-1-4 block-m-1-2 block-mob-full" data-aos="fade-up">
    
    
    
                <div class="col-block stats__col ">
    
                    <div class="stats__count">129</div>
    
                    <h5>Tim Website</h5>
    
                </div>
    
                <div class="col-block stats__col">
    
                    <div class="stats__count">1507</div>
    
                    <h5>Jumlah Individu</h5>
    
                </div>
    
                <div class="col-block stats__col">
    
                    <div class="stats__count">108</div>
    
                    <h5>Tim Gundu</h5>
    
                </div>
    
                <div class="col-block stats__col">
    
                    <div class="stats__count">103</div>
    
                    <h5>Tim Balap Karung</h5>
    
                </div>
    
                <div class="row">
    
                <div class="col-full">
    
                <a class="btn btn--stroke full-width" href="" style="white;">REGISTER</a>
    
                </div>
    
                </div>
    
            </div> <!-- end stats -->
    
    
    
        </section> <!-- end s-stats -->
    
        <!-- contact
    
        ================================================== -->
    
        <section id="contact" class="s-contact">
    
            <div class="row">
    
                  <p>
    
                  <a href="mailto:#0" class="contact-email">hello@transcend-studio.com</a>
    
                  <span class="contact-number">+1 (917) 123 456  /  +1 (917) 333 987</span>
    
                  </p>
    
              <!-- end contact-main -->
    
                <div class="col-five tab-full contact-secondary" data-aos="fade-up">
    
                    <h3 class="subhead subhead--light">Where To Find Us</h3>
    
    
    
                    <p class="contact-address">
    
                        1600 Amphitheatre Parkway<br>
    
                        Mountain View, CA<br>
    
                        94043 US
    
                    </p>
    
                </div> <!-- end contact-secondary -->
    
    
    
                <div class="col-five tab-full contact-secondary" data-aos="fade-up">
    
                    <h3 class="subhead subhead--light">Follow Us</h3>
    
    
    
                    <ul class="contact-social">
    
                        <li>
    
                            <a href="#0"><i class="fab fa-facebook"></i></a>
    
                        </li>
    
                        <li>
    
                            <a href="#0"><i class="fab fa-twitter"></i></a>
    
                        </li>
    
                        <li>
    
                            <a href="#0"><i class="fab fa-instagram"></i></a>
    
                        </li>
    
                        <li>
    
                            <a href="#0"><i class="fab fa-behance"></i></a>
    
                        </li>
    
                        <li>
    
                            <a href="#0"><i class="fab fa-dribbble"></i></a>
    
                        </li>
    
                    </ul> <!-- end contact-social -->
    
    
    
                    <div class="contact-subscribe">
    
                        <form id="mc-form" class="group mc-form" novalidate="true">
    
                            <input type="email" value="" name="EMAIL" class="email" id="mc-email" placeholder="Email Address" required="">
    
                            <input type="submit" name="subscribe" value="Subscribe">
    
                            <label for="mc-email" class="subscribe-message"></label>
    
                        </form>
    
                    </div> <!-- end contact-subscribe -->
    
                </div> <!-- end contact-secondary -->
    
    
    
            </div> <!-- end row -->
    
    
    
            <div class="row">
    
                <div class="col-full cl-copyright">
    
                    <span><!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
    
    Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | This template is made with <i class="fa fa-heart" aria-hidden="true"></i> by <a href="https://colorlib.com" target="_blank">Colorlib</a>
    
    <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. --></span>
    
                </div>
    
            </div>
    
    
    
            <div class="cl-go-top">
    
                <a class="smoothscroll" title="Back to Top" href="#top"><i class="icon-arrow-up" aria-hidden="true"></i></a>
    
            </div>
    
    
    
        </section> <!-- end s-contact -->
    
    
    
    
    
        <!-- photoswipe background
    
        ================================================== -->
    
        <div aria-hidden="true" class="pswp" role="dialog" tabindex="-1">
    
    
    
            <div class="pswp__bg"></div>
    
            <div class="pswp__scroll-wrap">
    
    
    
                <div class="pswp__container">
    
                    <div class="pswp__item"></div>
    
                    <div class="pswp__item"></div>
    
                    <div class="pswp__item"></div>
    
                </div>
    
    
    
                <div class="pswp__ui pswp__ui--hidden">
    
                    <div class="pswp__top-bar">
    
                        <div class="pswp__counter"></div><button class="pswp__button pswp__button--close" title="Close (Esc)"></button> <button class="pswp__button pswp__button--share" title=
    
                        "Share"></button> <button class="pswp__button pswp__button--fs" title="Toggle fullscreen"></button> <button class="pswp__button pswp__button--zoom" title=
    
                        "Zoom in/out"></button>
    
                        <div class="pswp__preloader">
    
                            <div class="pswp__preloader__icn">
    
                                <div class="pswp__preloader__cut">
    
                                    <div class="pswp__preloader__donut"></div>
    
                                </div>
    
                            </div>
    
                        </div>
    
                    </div>
    
                    <div class="pswp__share-modal pswp__share-modal--hidden pswp__single-tap">
    
                        <div class="pswp__share-tooltip"></div>
    
                    </div><button class="pswp__button pswp__button--arrow--left" title="Previous (arrow left)"></button> <button class="pswp__button pswp__button--arrow--right" title=
    
                    "Next (arrow right)"></button>
    
                    <div class="pswp__caption">
    
                        <div class="pswp__caption__center"></div>
    
                    </div>
    
                </div>
    
    
    
            </div>
    
    
    
        </div> <!-- end photoSwipe background -->
    
    
    
    
    
        <!-- preloader
    
        ================================================== -->
    
        <div id="preloader">
    
            <div id="loader">
    
            </div>
    
        </div>
    
    
    
    
    
        <!-- Java Script
    
        ================================================== -->
    
        <script src="js/jquery-3.2.1.min.js"></script>
    
        <script src="js/plugins.js"></script>
    
        <script src="js/main.js"></script>
        @endsection