@extends('layouts.app')

@section('content')
<!-- Main wrapper  -->
<div id="main-wrapper">
        <!-- header header  -->
        <div class="header">
            <nav class="navbar top-navbar navbar-expand-md navbar-light">
                <!-- Logo -->
                <div class="navbar-header">
                    <a class="navbar-brand" href="index.html">
                        <!-- Logo icon -->
                        <b><img style="width:50px;height:50px" src="" alt="" class="dark-logo" /></b>
                        <!--End Logo icon -->
                        <!-- Logo text -->
                        <span><img style="width:70px;height:70px"  src="images/integer.png" alt="" class="dark-logo" /></span>
                    </a>
                </div>
                <!-- End Logo -->
                <div class="navbar-collapse">
                    <!-- toggle and nav items -->
                    <ul class="navbar-nav mr-auto mt-md-0">
                        <!-- This is  -->
                        <li class="nav-item"> <a class="nav-link nav-toggler hidden-md-up text-muted  " href="javascript:void(0)"><i class="mdi mdi-menu"></i></a> </li>
                        <li class="nav-item m-l-10"> <a class="nav-link sidebartoggler hidden-sm-down text-muted  " href="javascript:void(0)"><i class="ti-menu"></i></a> </li>                        
                    </ul>
                    <!-- User profile and search -->
                    <ul class="navbar-nav my-lg-0">

                        <!-- Search -->                        
                        <!-- Comment -->
                        <!-- Profile -->
                        <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle text-muted  " href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><img src="images/users/user.png" alt="user" class="profile-pic" /></a>
                            <div class="dropdown-menu dropdown-menu-right animated zoomIn">
                                <ul class="dropdown-user">                                    
                                    <li><a href="#"><i class="ti-settings"></i> Ganti Password</a></li>
                                    <li><a href="{{ url('logout') }}"
                                        onclick="event.preventDefault();
                                                 document.getElementById('logout-form').submit();">
                                        Logout
                                    </a></li>
                                </ul>
                            </div>
                        </li>
                    </ul>
                </div>
            </nav>
        </div>
        <!-- End header header -->
        <!-- Left Sidebar  -->
        <div class="left-sidebar">
            <!-- Sidebar scroll-->
            <div class="scroll-sidebar">
                <!-- Sidebar navigation-->
                <nav class="sidebar-nav">
                <ul id="sidebarnav">
                        <li class="nav-devider"></li>
                        <li class="nav-label">Home</li>
                        <li> <a class="" href="{{url("home")}}" aria-expanded="false"><i class="fa fa-tachometer"></i><span class="hide-menu">Dashboard</span></a></li>
                        <li class="nav-label">Pembayaran</li>
                        <li> <a class=" " href="{{url("uploadkwin")}}" aria-expanded="false"><i class="fa fa-bank"></i><span class="hide-menu">Upload Pembayaran</span></a>
                        </li>                        
                        <li class="nav-label">Data Tim</li>
                        <li> <a class="  " href="{{url("updatedata")}}" aria-expanded="false"><i class="fa fa-address-book"></i><span class="hide-menu">Update Data Tim</span></a>                            
                        </li>
                        <li class="nav-label">Upload Berkas</li>
						<li> <a class="  " href="{{url('uploadproposal')}}" aria-expanded="false"><i class="fa fa-suitcase"></i><span class="hide-menu">Upload Proposal</span></a>                            
                        </li>
                        <li> <a class="  " href="{{url('uploadproject')}}" aria-expanded="false"><i class="fa fa-wpforms"></i><span class="hide-menu">Upload Project</span></a>                            
                        </li>                                                
                    </ul>
                </nav>
                <!-- End Sidebar navigation -->
            </div>
            <!-- End Sidebar scroll-->
        </div>
        <!-- End Left Sidebar  -->
        <!-- Page wrapper  -->
        <div class="page-wrapper">
            <!-- Bread crumb -->
            <div class="row page-titles">
                <div class="col-md-5 align-self-center">
                    <h3 class="text-primary">Link GDrive Project</h3> </div>
                <div class="col-md-7 align-self-center">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                        <li class="breadcrumb-item active">Link GDrive Project</li>
                    </ol>
                </div>
            </div>
            <!-- End Bread crumb -->
            <!-- Container fluid  -->
            <div class="container-fluid">
                <!-- Start Page Content -->
                <div class="row">
                    <div class="col-12">                    
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title">Link GDrive Project</h4>
                                @if (Session::has('message'))
                                <div class="alert alert-danger" role="alert">
                                    {{Session::get('message')}}
                                </div>
                                @endif
                                <div class="table-responsive m-t-40">                              
                                @if ($stat == 1)
                                    <p>Data Telah Tersimpan</p>
                                    <a target="_blank" style="color:green;" href="{{$linkproposal}}"> Link Berkas Anda</a>

                                @else
                                    <form action="{{url("uploadproject")}}" id="updatetim123" method="post">
                                    <center>
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Link GDrive</label>
                                        {{csrf_field()}}
                                        <input  name="linkproject" type="text" value="" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Link">                                    
                                    </div>
                                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal">
                                        Simpan
                                    </button>
                                @endif
                                
                                </center>
                                </form> 
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- End PAge Content -->
            </div>
            <form id="logout-form" action="{{ url('logout') }}" method="POST" style="display: none;">
                {{ csrf_field() }}
            </form>
            <!-- End Container fluid  -->
            <!-- footer -->            
            <!-- End footer -->
        </div>
        <!-- End Page wrapper  -->
    </div>
    <div class="modal" id="myModal" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document" style="display:table;">
            <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Konfirmasi</h5>                
                </button>
            </div>
            <div class="modal-body">
                <div class="container">
                <div class="row">
                <p>Apakah anda yakin ? Jika anda menekan iya maka data yang anda upload akan dikunci</p>
                </div>
                </div>
            </div>
            <div class="modal-footer">                
                <button type="button" class="btn btn-secondary" id="submit1" >Iya</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Tidak</button>
            </div>
            </div>
        </div>
    </div>    
    <!-- End Wrapper -->
@endsection
