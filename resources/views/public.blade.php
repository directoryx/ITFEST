@extends('layouts.pub')

@section('content')
<body>

  <div class="clearfix borderbox" id="page"><!-- column -->
   <div class="position_content" id="page_position_content">
    <div class="browser_width colelem" id="u94-bw">
     <div id="u94"><!-- group -->
      <div class="clearfix" id="u94_align_to_page">
       <div class="clip_frame grpelem" id="u205"><!-- image -->
        <img class="block" id="u205_img" src="images/integer.png?crc=4107151771" alt="" data-image-width="64" data-image-height="50"/>
       </div>
       <div class="clearfix grpelem" id="u227-4"><!-- content -->
        <a href="https://integer.lug-surabaya.com/register">Registration</a>
       </div>
       <div class="clearfix grpelem" id="u230-4"><!-- content -->
        <a href="https://integer.lug-surabaya.com/login">Login</a>
       </div>
      </div>
     </div>
    </div>
    <div class="browser_width colelem" id="u243-bw">
     <div class="museBGSize" id="u243"><!-- simple frame --></div>
    </div>
    <div class="clearfix colelem" id="pu285"><!-- group -->
     <div class="browser_width grpelem" id="u285-bw">
      <div id="u285"><!-- column -->
       <div class="clearfix" id="u285_align_to_page">
        <img class="colelem" id="u288-4" alt="COMPETITION &amp; EVENTS" src="images/u288-4.png?crc=63120564" data-image-width="593"/><!-- rasterized frame -->
        <div class="clearfix colelem" id="pu312"><!-- group -->
         <div class="shadow gradient rounded-corners clearfix grpelem" id="u312"><!-- group -->
          <div class="clip_frame grpelem" id="u300"><!-- image -->
           <img class="block" id="u300_img" src="images/792753_cup_512x512.png?crc=184407372" alt="" data-image-width="160" data-image-height="160"/>
          </div>
          <div class="clearfix grpelem" id="pu315-4"><!-- column -->
           <div class="clearfix colelem" id="u315-4"><!-- content -->
            <p>Competition 1</p>
           </div>
           <div class="clearfix colelem" id="u318-4"><!-- content -->
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
           </div>
           <div class="clearfix colelem" id="u322-4"><!-- content -->
            <p><span id="u322">Read More &gt;&gt;</span></p>
           </div>
          </div>
         </div>
         <div class="shadow gradient rounded-corners clearfix grpelem" id="u325"><!-- group -->
          <div class="clip_frame grpelem" id="u326"><!-- image -->
           <img class="block" id="u326_img" src="images/792753_cup_512x512.png?crc=184407372" alt="" data-image-width="160" data-image-height="160"/>
          </div>
          <div class="clearfix grpelem" id="pu328-4"><!-- column -->
           <div class="clearfix colelem" id="u328-4"><!-- content -->
            <p>Competition 2</p>
           </div>
           <div class="clearfix colelem" id="u329-4"><!-- content -->
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
           </div>
           <div class="clearfix colelem" id="u330-4"><!-- content -->
            <p><span id="u330">Read More &gt;&gt;</span></p>
           </div>
          </div>
         </div>
        </div>
        <div class="clearfix colelem" id="pu344"><!-- group -->
         <div class="shadow gradient rounded-corners clearfix grpelem" id="u344"><!-- group -->
          <div class="clip_frame grpelem" id="u382"><!-- image -->
           <img class="block" id="u382_img" src="images/calendar.png?crc=4168481822" alt="" data-image-width="158" data-image-height="158"/>
          </div>
          <div class="clearfix grpelem" id="pu347-4"><!-- column -->
           <div class="clearfix colelem" id="u347-4"><!-- content -->
            <p>Event 1</p>
           </div>
           <div class="clearfix colelem" id="u348-4"><!-- content -->
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
           </div>
           <div class="clearfix colelem" id="u349-4"><!-- content -->
            <p><span id="u349">Read More &gt;&gt;</span></p>
           </div>
          </div>
         </div>
         <div class="shadow gradient rounded-corners clearfix grpelem" id="u399"><!-- group -->
          <div class="clip_frame grpelem" id="u403"><!-- image -->
           <img class="block" id="u403_img" src="images/calendar.png?crc=4168481822" alt="" data-image-width="158" data-image-height="158"/>
          </div>
          <div class="clearfix grpelem" id="pu400-4"><!-- column -->
           <div class="clearfix colelem" id="u400-4"><!-- content -->
            <p>Event 2</p>
           </div>
           <div class="clearfix colelem" id="u401-4"><!-- content -->
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
           </div>
           <div class="clearfix colelem" id="u402-4"><!-- content -->
            <p><span id="u402">Read More &gt;&gt;</span></p>
           </div>
          </div>
         </div>
        </div>
       </div>
      </div>
     </div>
     <div class="browser_width grpelem" id="u270-bw">
      <div class="shadow" id="u270"><!-- group -->
       <div class="clearfix" id="u270_align_to_page">
        <div class="clearfix grpelem" id="u273-4"><!-- content -->
         <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
        </div>
       </div>
      </div>
     </div>
    </div>
    <div class="verticalspacer" data-offset-top="1770" data-content-above-spacer="1769" data-content-below-spacer="142"></div>
    <div class="browser_width colelem" id="u418-bw">
     <div class="shadow rounded-corners" id="u418"><!-- group -->
      <div class="clearfix" id="u418_align_to_page">
       <div class="clearfix grpelem" id="pu460"><!-- column -->
        <div class="clip_frame colelem" id="u460"><!-- image -->
         <img class="block" id="u460_img" src="images/icons8_facebook_64px.png?crc=113486579" alt="" data-image-width="44" data-image-height="44"/>
        </div>
        <div class="clip_frame colelem" id="u450"><!-- image -->
         <img class="block" id="u450_img" src="images/icons8_instagram_64px_2.png?crc=4115037565" alt="" data-image-width="44" data-image-height="44"/>
        </div>
       </div>
       <div class="clearfix grpelem" id="pu470-4"><!-- column -->
        <div class="clearfix colelem" id="u470-4"><!-- content -->
         <p>itfeststikomsby</p>
        </div>
        <div class="clearfix colelem" id="u473-4"><!-- content -->
         <p>@itfeststikomsby</p>
        </div>
       </div>
       <div class="clearfix grpelem" id="pu476"><!-- column -->
        <div class="clip_frame colelem" id="u476"><!-- image -->
         <img class="block" id="u476_img" src="images/icons8_twitter_64px.png?crc=275926592" alt="" data-image-width="44" data-image-height="44"/>
        </div>
        <div class="clip_frame colelem" id="u486"><!-- image -->
         <img class="block" id="u486_img" src="images/icons8_line_64px_1.png?crc=3917144425" alt="" data-image-width="44" data-image-height="44"/>
        </div>
       </div>
       <div class="clearfix grpelem" id="pu427-4"><!-- column -->
        <div class="clearfix colelem" id="u427-4"><!-- content -->
         <p>For more information :</p>
        </div>
        <div class="clearfix colelem" id="pu526-4"><!-- group -->
         <div class="clearfix grpelem" id="u526-4"><!-- content -->
          <p>@itfeststikomsby</p>
         </div>
         <div class="clip_frame grpelem" id="u496"><!-- image -->
          <img class="block" id="u496_img" src="images/icons8_message_52px.png?crc=3821194185" alt="" data-image-width="44" data-image-height="44"/>
         </div>
        </div>
        <div class="clearfix colelem" id="pu529-4"><!-- group -->
         <div class="clearfix grpelem" id="u529-4"><!-- content -->
          <p>itfeststikom_sub</p>
         </div>
         <div class="clip_frame grpelem" id="u516"><!-- image -->
          <img class="block" id="u516_img" src="images/icons8_address_48px.png?crc=4067236084" alt="" data-image-width="44" data-image-height="44"/>
         </div>
        </div>
       </div>
       <div class="clearfix grpelem" id="pu532-4"><!-- column -->
        <div class="clearfix colelem" id="u532-4"><!-- content -->
         <p>itfeststikomsby@gmail.com</p>
        </div>
        <div class="clearfix colelem" id="u535-4"><!-- content -->
         <p>Jalan Raya Kedung Baruk No. 98, Kedung Baruk, Rungkut, Kota Surabaya, Jawa Timur 60298</p>
        </div>
       </div>
      </div>
     </div>
    </div>
   </div>
  </div>
  <!-- Other scripts -->
  <script type="text/javascript">
   window.Muse.assets.check=function(d){if(!window.Muse.assets.checked){window.Muse.assets.checked=!0;var b={},c=function(a,b){if(window.getComputedStyle){var c=window.getComputedStyle(a,null);return c&&c.getPropertyValue(b)||c&&c[b]||""}if(document.documentElement.currentStyle)return(c=a.currentStyle)&&c[b]||a.style&&a.style[b]||"";return""},a=function(a){if(a.match(/^rgb/))return a=a.replace(/\s+/g,"").match(/([\d\,]+)/gi)[0].split(","),(parseInt(a[0])<<16)+(parseInt(a[1])<<8)+parseInt(a[2]);if(a.match(/^\#/))return parseInt(a.substr(1),
16);return 0},g=function(g){for(var f=document.getElementsByTagName("link"),h=0;h<f.length;h++)if("text/css"==f[h].type){var i=(f[h].href||"").match(/\/?css\/([\w\-]+\.css)\?crc=(\d+)/);if(!i||!i[1]||!i[2])break;b[i[1]]=i[2]}f=document.createElement("div");f.className="version";f.style.cssText="display:none; width:1px; height:1px;";document.getElementsByTagName("body")[0].appendChild(f);for(h=0;h<Muse.assets.required.length;){var i=Muse.assets.required[h],l=i.match(/([\w\-\.]+)\.(\w+)$/),k=l&&l[1]?
l[1]:null,l=l&&l[2]?l[2]:null;switch(l.toLowerCase()){case "css":k=k.replace(/\W/gi,"_").replace(/^([^a-z])/gi,"_$1");f.className+=" "+k;k=a(c(f,"color"));l=a(c(f,"backgroundColor"));k!=0||l!=0?(Muse.assets.required.splice(h,1),"undefined"!=typeof b[i]&&(k!=b[i]>>>24||l!=(b[i]&16777215))&&Muse.assets.outOfDate.push(i)):h++;f.className="version";break;case "js":h++;break;default:throw Error("Unsupported file type: "+l);}}d?d().jquery!="1.8.3"&&Muse.assets.outOfDate.push("jquery-1.8.3.min.js"):Muse.assets.required.push("jquery-1.8.3.min.js");
f.parentNode.removeChild(f);if(Muse.assets.outOfDate.length||Muse.assets.required.length)f="Some files on the server may be missing or incorrect. Clear browser cache and try again. If the problem persists please contact website author.",g&&Muse.assets.outOfDate.length&&(f+="\nOut of date: "+Muse.assets.outOfDate.join(",")),g&&Muse.assets.required.length&&(f+="\nMissing: "+Muse.assets.required.join(","))};location&&location.search&&location.search.match&&location.search.match(/muse_debug/gi)?setTimeout(function(){g(!0)},5E3):g()}};
var muse_init=function(){require.config({baseUrl:""});require(["jquery","museutils","whatinput","jquery.watch","jquery.musepolyfill.bgsize"],function(d){var $ = d;$(document).ready(function(){try{
window.Muse.assets.check($);/* body */
Muse.Utils.transformMarkupToFixBrowserProblemsPreInit();/* body */
Muse.Utils.prepHyperlinks(true);/* body */
Muse.Utils.resizeHeight('.browser_width');/* resize height */
Muse.Utils.requestAnimationFrame(function() { $('body').addClass('initialized'); });/* mark body as initialized */
Muse.Utils.fullPage('#page');/* 100% height page */
Muse.Utils.showWidgetsWhenReady();/* body */
Muse.Utils.transformMarkupToFixBrowserProblems();/* body */
}catch(b){if(b&&"function"==typeof b.notify?b.notify():Muse.Assert.fail("Error calling selector function: "+b),false)throw b;}})})};

</script>
<!-- RequireJS script -->
<script src="scripts/require.js?crc=244322403" type="text/javascript" async data-main="scripts/museconfig.js?crc=168988563" onload="if (requirejs) requirejs.onError = function(requireType, requireModule) { if (requireType && requireType.toString && requireType.toString().indexOf && 0 <= requireType.toString().indexOf('#scripterror')) window.Muse.assets.check(); }" onerror="window.Muse.assets.check();"></script>
</body>
@endsection