@extends('layouts.daftar')

@section('content')
<div id="main-wrapper">

<div class="unix-login">
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-lg-4">
                <div class="login-content card">
                    <div class="login-form">
                        <h4><img style="width:200px;height:200px" src="images/integer.png"></h4>
                        <form action="{{url("register")}}" method="post"  enctype="application/x-www-form-urlencoded">
                            <div class="form-group">
                                <label>Nama Tim</label>
                                <input name="namatim" type="text" class="form-control" placeholder="Nama Tim" required>
                            </div>
                            <div class="form-group">
                                <label>Nama Ketua</label>
                                <input name="namaketua" type="text" class="form-control" placeholder="Nama Ketua" required>
                                {{csrf_field()}}
                            </div>
                            <div class="form-group">
                                <label>Alamat Email</label>
                                <input name="email" type="email" class="form-control" placeholder="Email" required>
                            </div>
                            <div class="form-group">
                                <label>Kata Sandi</label>
                                <input name="password" type="password" class="form-control" placeholder="Password" required>
                            </div>
                            <div class="form-group">
                                <label for="password-confirm">Konfirmasi Kata Sandi</label>
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" placeholder="Konfirmasi Password" required>
                            </div>
                            <div class="form-group">
                                <label>Nomer Handphone</label>
                                <input name="nohp" type="number" class="form-control" placeholder="Nomer Handphone" required>
                            </div>
                            <div class="form-group">
                                <label>Universitas</label>
                                <input name="universitas" type="text" class="form-control" placeholder="Universitas" required>
                            </div>
                            
                            <button type="submit" class="btn btn-primary btn-flat m-b-30 m-t-30">Daftar</button>
                            <div class="register-link m-t-15 text-center">
                                <a href="{{url('login')}}"> Masuk Disini</a>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

</div>
@endsection
