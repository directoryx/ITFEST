@extends('layouts.login')

@section('content')
<div id="main-wrapper">

<div class="unix-login">
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-lg-4">
                <div class="login-content card">
                    <div class="login-form">
                        <h4><img style="width:200px;height:200px" src="images/integer.png"></h4>
                        @if (Session::has('message'))
                            <div class="alert alert-info">{{ Session::get('message') }}</div>
                        @endif
                        <form action="{{url('login')}}" method="post">
                            <div class="form-group">
                                <label>Alamat Email</label>
                                <input name="email" type="email" class="form-control" placeholder="Email">
                                {{csrf_field()}}
                            </div>
                            <div class="form-group">
                                <label>Kata Sandi</label>
                                <input name="password" type="password" class="form-control" placeholder="Password">
                            </div>
                            <div class="checkbox">
                                

                            </div>
                            <button type="submit" class="btn btn-primary btn-flat m-b-30 m-t-30">Masuk</button>
                            <div class="register-link m-t-15 text-center">
                                <a href="{{url('register')}}"> Daftar Disini</a>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

</div>

@endsection
