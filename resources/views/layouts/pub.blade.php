<!DOCTYPE html>
<html class="nojs html css_verticalspacer" lang="en-US">
 <head>

  <meta http-equiv="Content-type" content="text/html;charset=UTF-8"/>
  <meta name="generator" content="2017.0.0.363"/>
  <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
  
  <script type="text/javascript">
  // Update the 'nojs'/'js' class on the html node
  document.documentElement.className = document.documentElement.className.replace(/\bnojs\b/g, 'js');

  // Check that all required assets are uploaded and up-to-date
  if(typeof Muse == "undefined") window.Muse = {}; window.Muse.assets = {"required":["js/museutils.js", "js/museconfig.js", "js/jquery.watch.js", "js/jquery.musepolyfill.bgsize.js", "js/require.js", "css/index.css"], "outOfDate":[]};
  </script>
  
  <title>Home</title>
  <!-- CSS -->
  <link rel="stylesheet" type="text/css" href="{{asset("css/site_global.css?crc=443350757")}}"/>
  <link rel="stylesheet" type="text/css" href="{{asset("css/index.css?crc=3833316185")}}" id="pagesheet"/>
  </head>
  @yield('content')
</html>