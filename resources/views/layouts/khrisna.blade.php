<!DOCTYPE html>

<html class="no-js" lang="en">

<head>



    <!--- basic page needs

    ================================================== -->

    <meta charset="utf-8">

    <title>INTEGER</title>

    <meta name="description" content="">

    <meta name="author" content="">



    <!-- mobile specific metas

    ================================================== -->

    <meta name="viewport" content="width=device-width, initial-scale=1">



    <!-- CSS

    ================================================== -->

    <link rel="stylesheet" href="css/base.css">

    <link rel="stylesheet" href="css/vendor.css">

    <link rel="stylesheet" href="css/main.css">



    <!-- script

    ================================================== -->

    <script src="js/modernizr.js"></script>

    <script src="js/pace.min.js"></script>



    <!-- favicons

    ================================================== -->

    <link rel="shortcut icon" href="" type="image/x-icon">

    <link rel="icon" href="" type="image/x-icon">



</head>



<body id="top">

@yield('content')
</body>



</html>
    