<!DOCTYPE html>
<html lang="en">

<head>
<meta charset="utf-8">
    <!--
            Proyek Ini Menggunakan Template Ela Admin

            https://github.com/puikinsh/ElaAdmin

            Terima Kasih Untuk Aigars Silkalns :)
    -->
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="images/integer.png">
    <title>INTEGER - Admin Panel</title>
    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
    <link href="{{asset("css/lib/chartist/chartist.min.css")}}" rel="stylesheet">
	<link href="{{asset("css/lib/owl.carousel.min.css")}}" rel="stylesheet" />
    <link href="{{asset("css/lib/owl.theme.default.min.css")}}" rel="stylesheet" />
    <link href="{{asset("css/themify-icons.css")}}" rel="stylesheet" />
    <!-- Bootstrap Core CSS -->
    <link href="{{asset("css/lib/bootstrap/bootstrap.min.css")}}" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="{{asset("css/helper.css")}}" rel="stylesheet">
    <link href="{{asset("css/style.css")}}" rel="stylesheet">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:** -->
    <!--[if lt IE 9]>
    <script src="https:**oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https:**oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
    
</head>

<body class="fix-header fix-sidebar">
    <!-- Preloader - style you can find in spinners.css -->
    <div class="preloader">
        <svg class="circular" viewBox="25 25 50 50">
			<circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" /> </svg>
    </div>
    <!-- Main wrapper  -->
    @yield('content')
    <!-- End Wrapper -->
    <!-- All Jquery -->
    <script src="{{asset("js/lib/jquery/jquery.min.js")}}"></script>
    <!-- Bootstrap tether Core JavaScript -->
    <script src="{{asset("js/lib/bootstrap/js/popper.min.js")}}"></script>
    <script src="{{asset("js/lib/bootstrap/js/bootstrap.min.js")}}"></script>
    <!-- slimscrollbar scrollbar JavaScript -->
    <script src="{{asset("js/jquery.slimscroll.js")}}"></script>
    <!--Menu sidebar -->
    <script src="{{asset("js/sidebarmenu.js")}}"></script>
    <!--stickey kit -->
    <script src="{{asset("js/lib/sticky-kit-master/dist/sticky-kit.min.js")}}"></script>
    <script src="{{asset("js/lib/datamap/d3.min.js")}}"></script>
    <script src="{{asset("js/lib/datamap/topojson.js")}}"></script>    
    <script src="{{asset("js/lib/weather/jquery.simpleWeather.min.js")}}"></script>
    <script src="{{asset("js/lib/weather/weather-init.js")}}"></script>
    <script src="{{asset("js/lib/owl-carousel/owl.carousel.min.js")}}"></script>
    <script src="{{asset("js/lib/owl-carousel/owl.carousel-init.js")}}"></script>
    <!--Custom JavaScript -->
    <script src="{{asset("js/custom.min.js")}}"></script>
    <script src="{{asset("js/lib/datatables/datatables.min.js")}}"></script>    
    <script src="{{asset("js/lib/datatables/datatables-init.js")}}"></script>
    <script>
    $( "#submit1" ).click(function() {
        $( "#updatetim123" ).submit();
    });
    </script>
    <script src="https://cdn.ckeditor.com/4.9.2/standard/ckeditor.js"></script>
    <script>    
        CKEDITOR.replace( 'editor1' );    
    </script>
</body>

</html>