@extends('layouts.admin')

@section('content')

<!-- Main wrapper  -->
<div id="main-wrapper">
        <!-- header header  -->
        <div class="header">
            <nav class="navbar top-navbar navbar-expand-md navbar-light">
                <!-- Logo -->
                
                <div class="navbar-header">
                    <a class="navbar-brand" href="index.html">
                        <!-- Logo icon -->
                        <b><img src="{{asset("images/logo.png")}}" alt="homepage" class="dark-logo" /></b>
                        <!--End Logo icon -->
                        <!-- Logo text -->
                        <span><img src="{{asset("images/logo-text.png")}}" alt="homepage" class="dark-logo" /></span>
                    </a>
                </div>
                <!-- End Logo -->
                <div class="navbar-collapse">
                    <!-- toggle and nav items -->
                    <ul class="navbar-nav mr-auto mt-md-0">
                        <!-- This is  -->
                        <li class="nav-item"> <a class="nav-link nav-toggler hidden-md-up text-muted  " href="javascript:void(0)"><i class="mdi mdi-menu"></i></a> </li>
                        <li class="nav-item m-l-10"> <a class="nav-link sidebartoggler hidden-sm-down text-muted  " href="javascript:void(0)"><i class="ti-menu"></i></a> </li>                        
                    </ul>
                    <!-- User profile and search -->
                    <ul class="navbar-nav my-lg-0">

                        
                        <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle text-muted  " href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><img src="{{asset("images/users/user.png")}}" alt="user" class="profile-pic" /></a>
                            <div class="dropdown-menu dropdown-menu-right animated zoomIn">
                                <ul class="dropdown-user">                                    
                                    <li><a href="#"><i class="ti-settings"></i> Ganti Password</a></li>
                                    <li><a href="{{ url('logout') }}"
                                        onclick="event.preventDefault();
                                                 document.getElementById('logout-form').submit();">
                                        Logout
                                    </a></li>
                                </ul>
                            </div>
                        </li>
                    </ul>
                </div>
            </nav>
        </div>
        <!-- End header header -->
        <!-- Left Sidebar  -->
        <div class="left-sidebar">
            <!-- Sidebar scroll-->
            <div class="scroll-sidebar">
                <!-- Sidebar navigation-->
                <nav class="sidebar-nav">
                <ul id="sidebarnav">
                        <li class="nav-devider"></li>
                        <li class="nav-label">Home</li>
                        <li> <a class="" href="{{url("pan/home")}}" aria-expanded="false"><i class="fa fa-tachometer"></i><span class="hide-menu">Dashboard</span></a></li>                                             
                        <li class="nav-label">Data Tim</li>
                        <li> <a class="  " href="{{url("pan/daftarpeserta")}}" aria-expanded="false"><i class="fa fa-address-book"></i><span class="hide-menu">Daftar Data Tim</span></a>                            
                        </li>
                        <li class="nav-label">Pengumuman</li>
						<li> <a class="  " href="{{url('pan/pengumuman')}}" aria-expanded="false"><i class="fa fa-suitcase"></i><span class="hide-menu">Daftar Pengumuman</span></a>                            
                        </li>                                              
                    </ul>
                </nav>
                <!-- End Sidebar navigation -->
            </div>
            <!-- End Sidebar scroll-->
        </div>
        <!-- End Left Sidebar  -->
        <!-- Page wrapper  -->
        <div class="page-wrapper">
            <!-- Bread crumb -->
            <div class="row page-titles">
                <div class="col-md-5 align-self-center">
                    <h3 class="text-primary">Dashboard</h3> </div>
                <div class="col-md-7 align-self-center">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                        <li class="breadcrumb-item active">Daftar Tim Peserta</li>
                    </ol>
                </div>
            </div>
            <!-- End Bread crumb -->
            <!-- Container fluid  -->
            <div class="container-fluid">
                <!-- Start Page Content -->
                <div class="row">
                    <div class="col-12">
                        @if (Session::has('message'))
                        <div class="alert alert-success" role="alert">
                            {{Session::get('message')}}
                        </div>
                        @endif
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title">Detail Tim</h4>
                                <div class="form-group">
                                    <label for="email">Nama Tim:</label>
                                    <input disabled type="text" class="form-control" id="email" value="{{$user->namatim}}">
                                </div>
                                <div class="form-group">
                                    <label for="pwd">Nama Ketua:</label>
                                    <input disabled type="text" class="form-control" id="pwd" value="{{$user->namaketua}}">
                                </div>

                                <div class="form-group">
                                    <label for="pwd">Email:</label>
                                    <input disabled type="text" class="form-control" id="pwd" value="{{$user->email}}">
                                </div>

                                <div class="form-group">
                                    <label for="pwd">No HP:</label>
                                    <input disabled type="text" class="form-control" id="pwd" value="{{$user->nohp}}">
                                </div>

                                <div class="form-group">
                                    <label for="pwd">Universitas:</label>
                                    <input disabled type="text" class="form-control" id="pwd" value="{{$user->universitas}}">
                                </div>
                                <hr />
                                @if (is_null($dataanggota))
                                    <p>Data Belum Diupdate</p>
                                @else
                                @php ($i = 1)
                                @foreach ($dataanggota as $dataanggotas)
                                <div class="form-group">
                                    <label for="pwd">Nama Anggota {{$i}} :</label>
                                    <input disabled type="text" class="form-control" id="pwd" value="{{$dataanggotas->namaanggota}}">
                                </div>
                                <div class="form-group">
                                    <label for="pwd">Email Anggota {{$i}}:</label>
                                    <input disabled type="text" class="form-control" id="pwd" value="{{$dataanggotas->emailanggota}}">
                                </div>
                                <div class="form-group">
                                    <label for="pwd">No Telp Anggota {{$i}} :</label>
                                    <input disabled type="text" class="form-control" id="pwd" value="{{$dataanggotas->notelpanggota}}">
                                </div>
                                @php ($i++)
                                @endforeach
                                
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title">Pembayaran</h4>
                                @if(is_null($databayar))
                                <p>Tim Belum Upload</p>
                                @elseif ($user->status == 1)
                                <div class="alert alert-danger" role="alert">
                                    Status : Belum Di Approve
                                </div>
                                <center><img style="width: 200px;height:200px" src="https://integer.lug-surabaya.com/{{$databayar->url}}"/>
                                <button type="button" class="btn btn-success pull-left" onclick="event.preventDefault();
                                                     document.getElementById('approve-form').submit();">Approve</button>
                                <button type="button" class="btn btn-danger pull-right" onclick="event.preventDefault();
                                                     document.getElementById('delete-form').submit();">Delete</button>
                                @else
                                <div class="alert alert-success" role="alert">
                                    Status : Sudah Di Approve
                                </div>
                                <center><img style="width: 200px;height:200px" src="https://integer.lug-surabaya.com/{{$databayar->url}}"/>
                                <button type="button" class="btn btn-success pull-left" onclick="event.preventDefault();
                                                 document.getElementById('approve-form').submit();">Approve</button>
                                <button type="button" class="btn btn-danger pull-right" onclick="event.preventDefault();
                                                 document.getElementById('delete-form').submit();">Delete</button>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title">Berkas Tim</h4>
                                @if(is_null($dataupload))
                                <p style="color:red;">Tim Belum Menyertakan Link!!</p>
                                @elseif (is_null($dataupload->linkproject))
                                <p style="color:green;">Link Proposal : {{$dataupload->linkproposal}}</p>
                                <p style="color:red;">Link Project : Belum Disertakan</p>
                                @else
                                <p style="color:green;">Link Proposal : <a href="{{$dataupload->linkproposal}}">Link</a></p>
                                <p style="color:green;">Link Project : <a href="{{$dataupload->linkproject}}">Link</a></p>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
                <!-- End PAge Content -->
            </div>
            <form id="logout-form" action="{{ url('pan/logout') }}" method="POST" style="display: none;">
                {{ csrf_field() }}
            </form>
            <form id="approve-form" action="{{ url('pan/daftarpeserta') }}" method="POST" style="display: none;">
                {{ csrf_field() }}
                <input type="hidden" name="kelompokid" value="{{$kelompokid_enc}}">
                <input type="hidden" name="act" value="{{$approve_enc}}">
            </form>
            <form id="delete-form" action="{{ url('pan/daftarpeserta') }}" method="POST" style="display: none;">
                {{ csrf_field() }}
                <input type="hidden" name="kelompokid" value="{{$kelompokid_enc}}">
                <input type="hidden" name="act" value="{{$delete_enc}}">
            </form>
            <!-- End Container fluid  -->
            <!-- footer -->            
            <!-- End footer -->
        </div>
        <!-- End Page wrapper  -->
    </div>
    <!-- End Wrapper -->
@endsection