<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Conten7t-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>INTEGER | Admin Login </title>

    <!-- Bootstrap -->
    <link href="{{asset("css/bootstrap/dist/css/bootstrap.min.css")}}" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="{{asset("css/font-awesome/css/font-awesome.min.css")}}" rel="stylesheet">
    <!-- NProgress -->
    <link href="{{asset("css/nprogress/nprogress.css")}}" rel="stylesheet">
    <!-- Animate.css -->
    <link href="{{asset("css/animate.css/animate.min.css")}}" rel="stylesheet">

    <!-- Custom Theme Style -->
    <link href="{{asset("css/custom.min.css")}}" rel="stylesheet">
  </head>
    @yield('content')


  <!-- jQuery -->
  <script src="{{asset("css/jquery/dist/jquery.min.js")}}"></script>
  <!-- Bootstrap -->
  <script src="{{asset("css/bootstrap/dist/js/bootstrap.min.js")}}"></script>
  <!-- FastClick -->
  <script src="{{asset("css/fastclick/lib/fastclick.js")}}"></script>
  <!-- NProgress -->
  <script src="{{asset("css/nprogress/nprogress.js")}}"></script>

  <!-- Custom Theme Scripts -->
  <script src="{{asset("css/custom.min.js")}}"></script>
  </body>
</html>