@extends('layouts.app')

@section('content')
<!-- Main wrapper  -->
<div id="main-wrapper">
        <!-- header header  -->
        <div class="header">
            <nav class="navbar top-navbar navbar-expand-md navbar-light">
                <!-- Logo -->
                <div class="navbar-header">
                    <a class="navbar-brand" href="index.html">
                        <!-- Logo icon -->
                        <b><img src="{{asset("images/logo.png")}}" alt="homepage" class="dark-logo" /></b>
                        <!--End Logo icon -->
                        <!-- Logo text -->
                        <span><img src="{{asset("images/logo-text.png")}}" alt="homepage" class="dark-logo" /></span>
                    </a>
                </div>
                <!-- End Logo -->
                <div class="navbar-collapse">
                    <!-- toggle and nav items -->
                    <ul class="navbar-nav mr-auto mt-md-0">
                        <!-- This is  -->
                        <li class="nav-item"> <a class="nav-link nav-toggler hidden-md-up text-muted  " href="javascript:void(0)"><i class="mdi mdi-menu"></i></a> </li>
                        <li class="nav-item m-l-10"> <a class="nav-link sidebartoggler hidden-sm-down text-muted  " href="javascript:void(0)"><i class="ti-menu"></i></a> </li>                        
                    </ul>
                    <!-- User profile and search -->
                    <ul class="navbar-nav my-lg-0">

                        
                        <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle text-muted  " href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><img src="{{asset("images/users/user.png")}}" alt="user" class="profile-pic" /></a>
                            <div class="dropdown-menu dropdown-menu-right animated zoomIn">
                                <ul class="dropdown-user">                                    
                                    <li><a href="#"><i class="ti-settings"></i> Ganti Password</a></li>
                                    <li><a href="{{ url('logout') }}"
                                        onclick="event.preventDefault();
                                                 document.getElementById('logout-form').submit();">
                                        Logout
                                    </a></li>
                                </ul>
                            </div>
                        </li>
                    </ul>
                </div>
            </nav>
        </div>
        <!-- End header header -->
        <!-- Left Sidebar  -->
        <div class="left-sidebar">
            <!-- Sidebar scroll-->
            <div class="scroll-sidebar">
                <!-- Sidebar navigation-->
                <nav class="sidebar-nav">
                <ul id="sidebarnav">
                        <li class="nav-devider"></li>
                        <li class="nav-label">Home</li>
                        <li> <a class="" href="{{url("pan/home")}}" aria-expanded="false"><i class="fa fa-tachometer"></i><span class="hide-menu">Dashboard</span></a></li>                                             
                        <li class="nav-label">Data Tim</li>
                        <li> <a class="  " href="{{url("pan/daftarpeserta")}}" aria-expanded="false"><i class="fa fa-address-book"></i><span class="hide-menu">Daftar Data Tim</span></a>                            
                        </li>
                        <li class="nav-label">Pengumuman</li>
						<li> <a class="  " href="{{url('pan/pengumuman')}}" aria-expanded="false"><i class="fa fa-suitcase"></i><span class="hide-menu">Daftar Pengumuman</span></a>                            
                        </li>                                              
                </ul>
                </nav>
                <!-- End Sidebar navigation -->
            </div>
            <!-- End Sidebar scroll-->
        </div>
        <!-- End Left Sidebar  -->
        <!-- Page wrapper  -->
        <div class="page-wrapper">
            <!-- Bread crumb -->
            <div class="row page-titles">
                <div class="col-md-5 align-self-center">
                    <h3 class="text-primary">Pengumuman</h3> </div>
                <div class="col-md-7 align-self-center">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                        <li class="breadcrumb-item active">Pengumuman</li>
                    </ol>
                </div>
            </div>
            <!-- End Bread crumb -->
            <!-- Container fluid  -->
            <div class="container-fluid">
                <!-- Start Page Content -->
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title">Pengumuman</h4>
                                <a class="btn btn-primary pull-right" href="{{url("pan/pengumuman/create")}}">Tambah Baru</a>
                                <div class="table-responsive m-t-40">
                                    
                                    <table id="example23" class="display nowrap table table-hover table-striped table-bordered" cellspacing="0" width="100%">
                                        
                                        <br/>
                                        <thead>
                                            <tr>
                                                <th>Nama Pengumuman</th>
                                                <th>Action</th>                                                
                                            </tr>
                                        </thead>                                       
                                        <tbody>
                                            @foreach ($berita as $user)
                                            <tr>
                                                <td>{{$user->namaberita}}</td>
                                                <td><!-- Button trigger modal -->
                                                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modal{{$user->id}}">
                                                    Lihat
                                                    </button>
                                                    <a href="pan/pengumuman/{{$user->id}}" onclick="event.preventDefault();
                                                    document.getElementById('delete-form{{$user->id}}').submit();" class="btn btn-danger">
                                                        Hapus
                                                    </a>
                                                </td>                                                
                                            </tr>                                            
                                            @endforeach                                                                                      
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- End PAge Content -->
            </div>
            <form id="logout-form" action="{{ url('logout') }}" method="POST" style="display: none;">
                {{ csrf_field() }}
            </form>
            <!-- End Container fluid  -->
            <!-- footer -->            
            <!-- End footer -->
        </div>
        @foreach ($berita as $user)

        <div class="modal" id="modal{{$user->id}}"tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document" style="display:table;">
            <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">{{$user->namaberita }}</h5>                
                </button>
            </div>
            <div class="modal-body">
                <div class="container">
                <div class="row">
                {!! $user->isi !!}
                </div>
                </div>
            </div>
            <div class="modal-footer">                
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
            </div>
        </div>
        </div>
        @endforeach
        <!-- End Page wrapper  -->
    </div>
    @foreach ($berita as $user)
    <form id="delete-form{{$user->id}}" action="pengumuman/{{$user->id}}" method="POST" style="display: none;">
        {{ csrf_field() }}
        <input type="hidden" name="_method" value="DELETE">
    </form>                    
    @endforeach                        
    <!-- End Wrapper -->
@endsection
