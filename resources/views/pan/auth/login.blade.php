@extends('pan.layout.auth')

@section('content')
<body class="login">
<div>
      <a class="hiddenanchor" id="signup"></a>
      <a class="hiddenanchor" id="signin"></a>

      <div class="login_wrapper">
        <div class="animate form login_form">
          <section class="login_content">
            <form action="{{url("pan/login")}}" method="post">
              <h1>Login Form</h1>
              <div>
                <input name="email" type="text" class="form-control" placeholder="Username" required="" />
                {{csrf_field()}}
              </div>
              <div>
                <input name="password" type="password" class="form-control" placeholder="Password" required="" />
              </div>
              <div>
                <button class="btn btn-default submit" href="#">Log in</a>                
              </div>
                         
            </form>
          </section>
        </div>        
      </div>
    </div>
</body>
@endsection
