<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'PublicController@index')->name('public');

Route::get('/uploadkwin', 'UploadController@index')->name('uploadkwin');
Route::post('/uploadkwin', 'UploadController@upload')->name('postuploadkwin');
Route::get('/updatedata', 'DataTimController@index')->name('updatedata');
Route::post('/updatedata', 'DataTimController@update')->name('postupdatedata');
Route::get('/uploadproposal', 'UploadBerkasController@index')->name('uploadberkas');
Route::post('/uploadproposal', 'UploadBerkasController@uploadproposal')->name('postuploadberkas');
Route::get('/uploadproject', 'ProjectUploadController@index')->name('uploadproject');
Route::post('/uploadproject', 'ProjectUploadController@uploadproject')->name('postuploadproject');

Route::group(['prefix' => 'pan'], function () {
  Route::get('/login', 'PanAuth\LoginController@showLoginForm')->name('login');  
  Route::post('/login', 'PanAuth\LoginController@login');
  Route::post('/logout', 'PanAuth\LoginController@logout')->name('logout');

  Route::resource('daftarpeserta', 'PanDaftarPesertaController');  
  Route::resource('pengumuman', 'PanPengumumanController');  

  Route::get('/asd', 'PanHomeController@index')->name('panindex');

  
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
