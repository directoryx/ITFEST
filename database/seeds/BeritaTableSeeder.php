<?php

use Illuminate\Database\Seeder;
use Faker\Generator as Faker;

class BeritaTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i=0;$i<=11;$i++){
            DB::table('berita')->insert([
                'namaberita' => str_random(10),
                'isi' => str_random(112),
            ]);
        }
        
    }
}
