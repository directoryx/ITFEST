<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDataUpload extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dataupload', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('kelompokid');
            $table->string('linkproposal')->nullable();
            $table->string('linkproject')->nullable();            
            $table->foreign('kelompokid')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dataupload');
    }
}
