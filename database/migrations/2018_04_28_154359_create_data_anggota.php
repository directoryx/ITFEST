<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDataAnggota extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dataanggota', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('kelompokid');
            $table->string('namaanggota');
            $table->string('emailanggota');
            $table->string('notelpanggota');
            $table->foreign('kelompokid')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dataanggota');
    }
}
